/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();


function moneyToFloat(value){
  return Number(value.substring(2).replace(',', '.'));
}
function floatToMoney(value){
  return ("R$" + value).replace('.', ',');
}

$(document).ready(function() {
	$('.datepicker').datepicker({format: 'dd/mm/yyyy'});
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.dropdown-trigger');
  var options = {
    alignment: 'top',
    inDuration: 500,
    outDuration: 500
  };
  var instances = M.Dropdown.init(elems, options);
});
// Or with jQuery
$('.dropdown-trigger').dropdown();

/*Formatando data*/
function formatDate(input) {
  var datePart = input.match(/\d+/g),
  year = datePart[0].substring(0), // get only two digits
  month = datePart[1], day = datePart[2];
  return day+'/'+month+'/'+year;
}

function formatDateReverse(input) {
  var datePart = input.match(/\d+/g),
  day = datePart[0].substring(0), // get only two digits
  month = datePart[1], year = datePart[2];
  return year+'-'+month+'-'+day;
}

/*Formatando número*/
function number_format (number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}

/*Formatando dinheiro negativo*/
function neg_money_format(str) {
  return '-' + str.replace('-', '');
}

/*Selecionando ícone da categoria*/
function icon_categoria(categoria){
  switch (categoria) {
    case '1':
      return 'restaurant';
      break;
    case '2':
      return 'school';
      break;
    case '3':
      return 'fitness_center';
      break;
    case '4':
      return 'weekend';
      break;
    case '5':
      return 'location_city';
      break;
    case '6':
      return 'widgets';
      break;
    case '7':
      return 'redeem';
      break;
    case '8':
      return 'local_mall';
      break;
    case '9':
      return 'local_atm';
      break;
    case '10':
      return 'local_hospital';
      break;
    case '11':
      return 'directions_bus';
      break;
    case '12':
      return 'flight';
      break;
    default:
      break;
  }
}

/*Selecionando ícone do tipo de movimentação*/
function icon_tipo(elemento, tipo){
  switch (tipo) {
    case '1':
      elemento.find(".tipo").addClass("receita");
      return 'arrow_drop_up';
      break;
    case '2':
      elemento.find(".tipo").addClass("despesa");
      return 'arrow_drop_down';
      break;
    default:
      break;
  }
}
