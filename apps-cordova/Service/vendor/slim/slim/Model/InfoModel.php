﻿<?php

class InfoModel extends Model {

    private $id;
    private $texto;
    private $titulo;

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getTexto() {
        return $this->texto;
    }
	
	
    public function setId($id) {
        $this->id = $id;
    }
	
    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setTexto($texto) {
        $this->texto = $texto;
    }	
	
	
    public function valida() {
        $error = true;

        if ($this->getTitulo() == "") {
            Message::setMessage('titulo', 'Titulo inválido', 'error');
            $error = false;
        }

        if ($this->getTexto() == "") {
            Message::setMessage('texto', 'Texto inválido', 'error');
            $error = false;
        }

        return $error;
    }

}