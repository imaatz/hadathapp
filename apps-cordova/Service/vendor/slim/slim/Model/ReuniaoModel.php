﻿<?php

class ReuniaoModel extends Model {

    private $id;
    private $descricao;
    private $titulo;
    private $dataHora;
    private $status;
    private $justificativa;
    private $usuario;

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getDescricao() {
        return $this->descricao;
    }
    
    public function getDataHora() {
        return $this->dataHora;
    }
    
    public function getJustificativa() {
        return $this->justificativa;
    }
    
    public function getStatus() {
        return $this->status;
    }
	
    public function getUsuario() {
        return $this->usuario;
    }
	
	
	
    public function setId($id) {
        $this->id = $id;
    }
	
    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setDataHora($dataHora) {
        $this->dataHora = $dataHora;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setJustificativa($justificativa) {
        $this->justificativa = $justificativa;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
    }	
	
	
	
    public function valida() {
        $error = true;

        if ($this->getTitulo() == "") {
            Message::setMessage('titulo', 'Titulo inválido', 'error');
            $error = false;
        }

        if ($this->getDescricao() == "") {
            Message::setMessage('descricao', 'Descricao inválido', 'error');
            $error = false;
        }

        if ($this->getDataHora() == "") {
            Message::setMessage('tipo', 'Nome de usuário inválido', 'error');
            $error = false;
        }

        return $error;
    }

}