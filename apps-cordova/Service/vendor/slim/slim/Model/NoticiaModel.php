﻿<?php

class NoticiaModel extends Model {

    private $id;
    private $titulo;
    private $descricaoBreve;
    private $texto;
    private $dataHora;
    private $dataHoraUpdate;

    public function getId() {
        return $this->id;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function getDescricaoBreve() {
        return $this->descricaoBreve;
    }

    public function getDataHora() {
        return $this->dateConverter($this->dataHora);
    }

    public function getDataHoraUpdate() {
        return $this->dateConverter($this->dataHoraUpdate);
    }

	
	
    public function setId($id) {
        $this->id = $id;
    }

    public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function setDataHora($dataHora) {
        $this->dataHora = $dataHora;
    }
	
    public function setTexto($texto) {
        $this->texto = $texto;
    }

    public function setDataHoraUpdate($dataHoraUpdate) {
        $this->dataHoraUpdate = $dataHoraUpdate;
    }
	
    public function setDescricaoBreve($descricaoBreve) {
        $this->descricaoBreve = $descricaoBreve;
    }	
	
	
	
    public function valida() {
        $error = true;

        if ($this->getTitulo() == "") {
            Message::setMessage('titulo', 'Titulo inválido', 'error');
            $error = false;
        }

        if ($this->getTexto() == "") {
            Message::setMessage('texto', 'Texto inválido', 'error');
            $error = false;
        }

        if ($this->getDescricaoBreve() == "") {
            Message::setMessage('descricaoBreve', 'Descrição inválida', 'error');
            $error = false;
        }

        return $error;
    }

}