<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';

$configs = [
  'settings' => [
    'displayErrorDetails' => true,
    'addContentLengthHeader' => false
  ],
];
$container = new \Slim\Container($configs);
$app = new \Slim\App($container);

//Login
$app->add(new \Tuupola\Middleware\HttpBasicAuthentication([

  "path" => "/auth",

  "authenticator" => function ($arguments) {
		require 'DAO/UsuarioDAO.php';
		require 'Serializers/UsuarioSerializer.php';
		require 'Model/UsuarioModel.php';
		$usuarioSerializer = new UsuarioSerializer();
		$usuarioDAO = new UsuarioDAO();
		$usuarioModel = new UsuarioModel();

		$usuarios = $usuarioSerializer->serializeList($usuarioDAO->getAll());
		$username = array();
		$senha = array();
		foreach ($usuarios as $usuario) {
			array_push($username, $usuario['username']);
			array_push($senha, $usuario['senha']);
		}
    return in_array($arguments['user'], $username) && in_array($arguments['password'], $senha)  ? (bool) 1 : (bool) 0;
  },

  "error" => function ($response, $arguments) {
    $data = [];
    $data["status"] = "error";
    $data["message"] = $arguments["message"];
    // $data["code"] = "401";
    return $response->write(json_encode($data, JSON_UNESCAPED_SLASHES));
  }
]));

$app->get('/auth', function (Request $request, Response $response) use ($app) {

  // $key = $this->get("secret");

  $now_seconds = time();
  $token = array(
    "iat" => $now_seconds,
    // "exp" => $now_seconds+(60*60),  // Maximum expiration time is one hour
    "exp" => $now_seconds+(15),
    "user" => 'teste',
    "password" => 'teste',
  );

  // $jwt = JWT::encode($token, $key,'HS384');

  //guardar token no bd do usuario e no bd do aplicativo

  return $response
  ->withJson(["auth-jwt" => "OK"], 200)
  ->withHeader('Content-type', 'application/json');
});

//GET
$app->get('/form', function (Request $request, Response $response) {
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$categorias = $categoriaSerializer->serializeList($categoriaDAO->getAll());

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('categorias' => $categorias, 'evento' => 0));
});

$app->get('/form/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$categorias = $categoriaSerializer->serializeList($categoriaDAO->getAll());
	$evento = $eventoSerializer->serialize($eventoDAO->getById($id));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('categorias' => $categorias, 'evento' => $evento));
});

$app->get('/usuario', function (Request $request, Response $response) {
	require 'DAO/UsuarioDAO.php';
	require 'Serializers/UsuarioSerializer.php';
	require 'Model/UsuarioModel.php';

	$usuarioModel = new UsuarioModel();
	$usuarioDAO = new UsuarioDAO();
	$usuarioSerializer = new UsuarioSerializer();

	$user = $_SERVER['PHP_AUTH_USER'];
	$usuario = $usuarioSerializer->serialize($usuarioDAO->getByUsername($user));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('usuario' => $usuario));
});

$app->get('/usuario/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

	require 'DAO/UsuarioDAO.php';
	require 'Serializers/UsuarioSerializer.php';
	require 'Model/UsuarioModel.php';

	$usuarioModel = new UsuarioModel();
	$usuarioSerializer = new UsuarioSerializer();
	$usuarioDAO = new UsuarioDAO();

	$usuario = $usuarioSerializer->serialize($usuarioDAO->getById($id));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('usuario' => $usuario));
});

$app->get('/eventos', function (Request $request, Response $response) {
	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	//Pegar eventos que ainda não aconteceram
	$eventos = $eventoSerializer->serializeList($eventoDAO->getTempo('futuro'));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}
	$categorias = $categoriaSerializer->serializeList($categoriaDAO->getAll());

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos, 'categorias' => $categorias));
});

$app->get('/evento/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$evento = $eventoSerializer->serialize($eventoDAO->getById($id));
	$categoria = $categoriaSerializer->serialize($categoriaDAO->getById($evento['idCategoria']));

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('evento' => $evento, 'categoria' => $categoria));
});

$app->get('/eventos/categoria/{categoria}/busca/{busca}/tempo/{tempo}', function (Request $request, Response $response) {
	$categoria = $request->getAttribute('categoria');
	$busca = $request->getAttribute('busca');
	$tempo = $request->getAttribute('tempo');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getCategoriaBuscaTempo($categoria, $busca, $tempo));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/categoria/{categoria}/busca/{busca}', function (Request $request, Response $response) {
	$categoria = $request->getAttribute('categoria');
	$busca = $request->getAttribute('busca');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getCategoriaBusca($categoria, $busca));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/categoria/{categoria}/tempo/{tempo}', function (Request $request, Response $response) {
	$categoria = $request->getAttribute('categoria');
	$tempo = $request->getAttribute('tempo');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getCategoriaTempo($categoria, $tempo));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/categoria/{categoria}', function (Request $request, Response $response) {
	$categoria = $request->getAttribute('categoria');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getCategoria($categoria));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/busca/{busca}/tempo/{tempo}', function (Request $request, Response $response) {
	$busca = $request->getAttribute('busca');
	$tempo = $request->getAttribute('tempo');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getBuscaTempo($busca, $tempo));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/busca/{busca}', function (Request $request, Response $response) {
	$busca = $request->getAttribute('busca');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getBusca($busca));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/tempo/{tempo}', function (Request $request, Response $response) {
	$tempo = $request->getAttribute('tempo');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();

	$eventos = $eventoSerializer->serializeList($eventoDAO->getTempo($tempo));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});

$app->get('/eventos/status/{status}', function (Request $request, Response $response) {
	$status = $request->getAttribute('status');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';
	require 'DAO/CategoriaDAO.php';
	require 'Serializers/CategoriaSerializer.php';
	require 'Model/CategoriaModel.php';
	require 'DAO/UsuarioDAO.php';
	require 'Serializers/UsuarioSerializer.php';
	require 'Model/UsuarioModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();
	$categoriaModel = new CategoriaModel();
	$categoriaSerializer = new CategoriaSerializer();
	$categoriaDAO = new CategoriaDAO();
	$usuarioDAO = new UsuarioDAO();
	$usuarioSerializer = new UsuarioSerializer();
	$usuarioModel = new UsuarioModel();

	$user = $_SERVER['PHP_AUTH_USER'];
	$usuario = $usuarioSerializer->serialize($usuarioDAO->getByUsername($user));

	//Pegar eventos que ainda não aconteceram
	$eventos = $eventoSerializer->serializeList($eventoDAO->getUsuarioStatus($usuario['id'], $status));
	$i=0;
	foreach ($eventos as $evento) {
		$categoria = $categoriaDAO->getById($evento['idCategoria']);
		$evento['cat_titulo'] = $categoria->getTitulo();
		$evento['cat_descricao'] = $categoria->getDescricao();
		$evento['cat_cor'] = $categoria->getCor();
		$eventos[$i] = $evento;
		$i++;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson(array('eventos' => $eventos));
});


//POST
$app->post('/confirmacaoPresenca/insert', function (Request $request, Response $response) {
	$json =  $request->getBody();
	$data = json_decode($json, true);

	require 'DAO/ConfirmacaoPresencaDAO.php';
	require 'Serializers/ConfirmacaoPresencaSerializer.php';
	require 'Model/ConfirmacaoPresencaModel.php';

	$usuarioDAO = new UsuarioDAO();
	$usuarioSerializer = new UsuarioSerializer();
	$confirmacaoPresencaDAO = new ConfirmacaoPresencaDAO();
	$confirmacaoPresencaModel = new ConfirmacaoPresencaModel();

	$user = $_SERVER['PHP_AUTH_USER'];
	$usuario = $usuarioSerializer->serialize($usuarioDAO->getByUsername($user));

	$confirmacaoPresencaModel->setIdUsuario($usuario["id"]);
	$confirmacaoPresencaModel->setIdEvento($data["idEvento"]);
	$confirmacaoPresencaModel->setPresenca($data["presenca"]);

	$erro = array();
	$valida = $confirmacaoPresencaModel->valida();
	if ($valida) {
		$erro[] = "Erro ao cadastrar presença";
	} else {
		$confirmacaoPresencaDAO->insert($confirmacaoPresencaModel);
	}

	return $response->withHeader('Acces-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/confirmacaoPresenca/update/{id}', function (Request $request, Response $response) {
	$json =  $request->getBody();
	$data = json_decode($json, true);
	$id = $request->getAttribute('id');

	require 'DAO/ConfirmacaoPresencaDAO.php';
	require 'Serializers/ConfirmacaoPresencaSerializer.php';
	require 'Model/ConfirmacaoPresencaoModel.php';

	$usuarioDAO = new UsuarioDAO();
	$usuarioSerializer = new UsuarioSerializer();
	$confirmacaoPresencaDAO = new ConfirmacaoPresencaDAO();
	$confirmacaoPresencaModel = new ConfirmacaoPresencaoModel();

	$user = $_SERVER['PHP_AUTH_USER'];
	$usuario = $usuarioSerializer->serialize($usuarioDAO->getByUsername($user));

	$confirmacaoPresencaModel->setId($id);
	$confirmacaoPresencaModel->setIdUsuario($usuario["id"]);
	$confirmacaoPresencaModel->setIdEvento($data["idEvento"]);
	$confirmacaoPresencaModel->setPresenca($data["presenca"]);

	$erro = array();
	$valida = $confirmacaoPresencaModel->valida();
	if ($valida) {
		$erro[] = "Erro ao cadastrar presença";
	} else {
		$confirmacaoPresencaDAO->insert($confirmacaoPresencaModel);
	}

	return $response->withHeader('Acces-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/eventos/insert', function (Request $request, Response $response) {
	$json = $request->getBody();
	$data = json_decode($json, true);

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';

	$eventoDAO = new EventoDAO();
	$eventoModel = new EventoModel();
	$eventoModel->setIdCategoria($data["idCategoria"]);
	$eventoModel->setTitulo($data["titulo"]);
	$eventoModel->setDatahora($data["datahora"]);
	$eventoModel->setDescricao($data["descricao"]);

	$erro = array();
	$valida = $eventoModel->valida();
	if($valida){
		$erro[] = "Erro ao cadastrar evento";
	} else {
		$eventoDAO->insert($eventoModel);
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/eventos/update/{id}', function (Request $request, Response $response) {
	$json = $request->getBody();
	$data = json_decode($json, true);
	$id = $request->getAttribute('id');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';

	$eventoDAO = new EventoDAO();
	$eventoModel = new EventoModel();
	$eventoModel->setId($id);
	$eventoModel->setTitulo($data["titulo"]);
	$eventoModel->setIdCategoria($data["idCategoria"]);
	$eventoModel->setDatahora($data["datahora"]);
	$eventoModel->setDescricao($data["descricao"]);

	$erro = array();
	$valida = $eventoModel->valida();
	if($valida){
		$erro[] = "Erro ao cadastrar evento";
	} else {
		$eventoDAO->update($eventoModel);
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/eventos/delete/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

	require 'DAO/EventoDAO.php';
	require 'Serializers/EventoSerializer.php';
	require 'Model/EventoModel.php';

	$eventoModel = new EventoModel();
	$eventoSerializer = new EventoSerializer();
	$eventoDAO = new EventoDAO();

	if($id==null){
		$erro[] = "Erro ao excluir evento";
	} else {
		$eventoDAO->delete($id);;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/usuarios/insert', function (Request $request, Response $response) {
	$json = $request->getBody();
	$data = json_decode($json, true);

	require 'DAO/UsuarioDAO.php';
	require 'Serializers/UsuarioSerializer.php';
	require 'Model/UsuarioModel.php';

	$usuarioDAO = new UsuarioDAO();
	$usuarioModel = new UsuarioModel();
	$usuarioModel->setId($data["id"]);
	$usuarioModel->setNome($data["nome"]);
	$usuarioModel->setUsername($data["username"]);
	$usuarioModel->setSenha($data["senha"]);

	$erro = array();
	$valida = $usuarioModel->valida();
	if($valida){
		$erro[] = "Erro ao cadastrar usuário";
	} else {
		$usuarioDAO->insert($usuarioModel);
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/usuarios/update/{id}', function (Request $request, Response $response) {
	$json = $request->getBody();
	$data = json_decode($json, true);
	$id = $request->getAttribute('id');

	require 'DAO/UsuarioDAO.php';
	require 'Serializers/UsuarioSerializer.php';
	require 'Model/UsuarioModel.php';

	$usuarioDAO = new UsuarioDAO();
	$usuarioModel = new UsuarioModel();
	$usuarioModel->setId($id);
	$usuarioModel->setNome($data["nome"]);
	$usuarioModel->setUsername($data["username"]);
	$usuarioModel->setSenha($data["senha"]);

	$erro = array();
	$valida = $eventoModel->valida();
	if($valida){
		$erro[] = "Erro ao cadastrar usuário";
	} else {
		$usuarioDAO->update($usuarioModel);
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->post('/usuarios/delete/{id}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

	require 'DAO/UsuarioDAO.php';
	require 'Serializers/UsuarioSerializer.php';
	require 'Model/UsuarioModel.php';

	$usuarioModel = new UsuarioModel();
	$usuarioSerializer = new UsuarioSerializer();
	$usuarioDAO = new UsuarioDAO();

	if($id==null){
		$erro[] = "Erro ao excluir usuário";
	} else {
		$usuarioDAO->delete($id);;
	}

	return $response->withHeader('Access-Control-Allow-Origin', '*')->withJson($erro);
});

$app->run();
