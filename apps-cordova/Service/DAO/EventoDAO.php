<?php
require_once "DAO.php";

class EventoDAO extends DAO {

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE id = :id
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setDescricao($row->DESCRICAO);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
        }
        return $eventoModel;
    }

    public function getCategoriaBuscaTempo($categoria, $busca, $tempo) {
        date_default_timezone_set('America/Sao_Paulo');
        $datahoraAtual = date('Y-m-d H:i:s');
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE idCategoria = :categoria
              AND titulo like :busca
              AND datahora " . ($tempo=='futuro'?'>':'<') . " \"" . $datahoraAtual . "\"
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':categoria', $categoria);
        $stmt->bindValue(':busca', '%' . $busca . '%');
        $stmt->execute();
        //die(var_dump($stmt->errorInfo()));

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getCategoriaBusca($categoria, $busca) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE idCategoria = :categoria
              AND titulo like :busca
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':categoria', $categoria);
        $stmt->bindValue(':busca', '%' . $busca . '%');
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getCategoriaTempo($categoria, $tempo) {
        date_default_timezone_set('America/Sao_Paulo');
        $datahoraAtual = date('Y-m-d H:i:s');
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE idCategoria = :categoria
              AND datahora " . ($tempo=='futuro'?'>':'<') . " \"" . $datahoraAtual . "\"
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':categoria', $categoria);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getCategoria($idCategoria) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE idCategoria = :idCategoria
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':idCategoria', $idCategoria);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getBuscaTempo($busca, $tempo) {
        date_default_timezone_set('America/Sao_Paulo');
        $datahoraAtual = date('Y-m-d H:i:s');

        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE titulo like :busca
              AND datahora " . ($tempo=='futuro'?'>':'<') . " \"" . $datahoraAtual . "\"
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':busca', '%' . $busca . '%');
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getBusca($busca) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE titulo like :busca
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':busca', '%' . $busca . '%');
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getTempo($tempo) {
        date_default_timezone_set('America/Sao_Paulo');
        $datahoraAtual = date('Y-m-d H:i:s');

        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento
            WHERE datahora " . ($tempo=='futuro'?'>':'<') . " \"" . $datahoraAtual . "\"
            ORDER BY datahora ASC
        ");
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function getUsuarioStatus($usuario, $status) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM evento e
            INNER JOIN confirmacao_presenca cp ON e.id = cp.idEvento
            WHERE cp.idUsuario = :usuario
              AND cp.presenca = :status
            ORDER BY datahora ASC
        ");
        $stmt->bindValue(':usuario', $usuario);
        $stmt->bindValue(':status', $status);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $eventoModel = new EventoModel;
            $eventoModel->setId($row->ID);
            $eventoModel->setTitulo($row->TITULO);
            $eventoModel->setIdCategoria($row->IDCATEGORIA);
            $eventoModel->setDatahora($row->DATAHORA);
            $eventoModel->setDescricao($row->DESCRICAO);
            $results[] = $eventoModel;
          }
        }
        return $results;
    }

    public function insert(EventoModel $eventoModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO evento (titulo, descricao, datahora, idCategoria)
                VALUES (:titulo, :descricao, :datahora, :idCategoria)
            ");
            $stmt->bindValue(':titulo', $eventoModel->getTitulo());
            $stmt->bindValue(':descricao', $eventoModel->getDescricao());
            $stmt->bindValue(':datahora', $eventoModel->getDatahora());
            $stmt->bindValue(':idCategoria', $eventoModel->getIdCategoria());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(EventoModel $eventoModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE evento
                SET titulo = :titulo,
                    descricao = :descricao,
                    datahora = :datahora,
                    idCategoria = :idCategoria
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $eventoModel->getId());
            $stmt->bindValue(':titulo', $eventoModel->getTitulo());
            $stmt->bindValue(':idCategoria', $eventoModel->getIdCategoria());
            $stmt->bindValue(':datahora', $eventoModel->getDatahora());
            $stmt->bindValue(':descricao', $eventoModel->getDescricao());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM evento
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }

        return true;
    }

}
