<?php

class DAO {

    public $conn;

    public function __construct() {
        $dsn = 'mysql:host=localhost;port=3306;dbname=dsiii_tarefa_4';
        $usuario = 'root';
        $senha = '';
        $opcoes = array(PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_CASE => PDO::CASE_UPPER
        );

        try {
            $this->conn = new PDO($dsn, $usuario, $senha, $opcoes);
        } catch (PDOException $e) {
            echo 'Erro: ' . $e->getMessage();
        }
    }

}
