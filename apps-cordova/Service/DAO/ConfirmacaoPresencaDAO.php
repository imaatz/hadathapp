<?php
require_once "DAO.php";

class ConfirmacaoPresencaDAO extends DAO {

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM confirmacao_presenca
            WHERE id = :id
            ORDER BY presenca DESC
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $confirmacaoPresencaModel = new ConfirmacaoPresencaModel;
            $confirmacaoPresencaModel->setId($row->ID);
            $confirmacaoPresencaModel->setIdUsuario($row->IDUSUARIO);
            $confirmacaoPresencaModel->setIdEvento($row->IDEVENTO);
            $confirmacaoPresencaModel->setPresenca($row->PRESENCA);
        }
        return $confirmacaoPresencaModel;
    }

    public function getByUsuario($usuario) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM confirmacao_presenca
            WHERE idUsuario = :idUsuario
            ORDER BY presenca DESC
        ");
        $stmt->bindValue(':idUsuario', $idUsuario);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $confirmacaoPresencaModel = new ConfirmacaoPresencaModel;
            $confirmacaoPresencaModel->setId($row->ID);
            $confirmacaoPresencaModel->setIdUsuario($row->IDUSUARIO);
            $confirmacaoPresencaModel->setIdEvento($row->IDEVENTO);
            $confirmacaoPresencaModel->setPresenca($row->PRESENCA);
            $results[] = $confirmacaoPresencaModel;
          }
        }
        return $results;
    }

    public function getByEvento($evento) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM confirmacao_presenca
            WHERE idEvento = :idEvento
            ORDER BY presenca DESC
        ");
        $stmt->bindValue(':idEvento', $idEvento);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $confirmacaoPresencaModel = new ConfirmacaoPresencaModel;
            $confirmacaoPresencaModel->setId($row->ID);
            $confirmacaoPresencaModel->setIdUsuario($row->IDUSUARIO);
            $confirmacaoPresencaModel->setIdEvento($row->IDEVENTO);
            $confirmacaoPresencaModel->setPresenca($row->PRESENCA);
            $results[] = $confirmacaoPresencaModel;
          }
        }
        return $results;
    }

    public function getByPresenca($presenca) {
        $results = array();
        $stmt = $this->conn->prepare("
          SELECT *
            FROM confirmacao_presenca
            WHERE presenca = :presenca
        ");
        $stmt->bindValue(':presenca', $presenca);
        $stmt->execute();

        if ($stmt) {
          while ($row = $stmt->fetchObject()) {
            $confirmacaoPresencaModel = new ConfirmacaoPresencaModel;
            $confirmacaoPresencaModel->setId($row->ID);
            $confirmacaoPresencaModel->setIdUsuario($row->IDUSUARIO);
            $confirmacaoPresencaModel->setIdEvento($row->IDEVENTO);
            $confirmacaoPresencaModel->setPresenca($row->PRESENCA);
            $results[] = $confirmacaoPresencaModel;
          }
        }
        return $results;
    }

    public function insert(ConfirmacaoPresencaModel $confirmacaoPresencaModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO confirmacao_presenca (idUsuario, idEvento, presenca)
                VALUES (:idUsuario, :idEvento, :presenca)
            ");
            $stmt->bindValue(':idUsuario', $confirmacaoPresencaModel->getIdUsuario());
            $stmt->bindValue(':idEvento', $confirmacaoPresencaModel->getIdEvento());
            $stmt->bindValue(':presenca', $confirmacaoPresencaModel->getPresenca());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(ConfirmacaoPresencaModel $confirmacaoPresencaModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE confirmacao_presenca
                SET idUsuario = :idUsuario,
                    idEvento = :idEvento,
                    presenca = :presenca
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $confirmacaoPresencaModel->getId());
            $stmt->bindValue(':idUsuario', $confirmacaoPresencaModel->getIdUsuario());
            $stmt->bindValue(':idEvento', $confirmacaoPresencaModel->getIdEvento());
            $stmt->bindValue(':presenca', $confirmacaoPresencaModel->getPresenca());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM confirmacao_presenca
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }

        return true;
    }

}
