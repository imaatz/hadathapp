<?php
require_once "DAO.php";

class UsuarioDAO extends DAO {

    public function getAll() {
        $results = array();
        $stmt = $this->conn->query("
          SELECT *
            FROM usuario
        ");
        if ($stmt) {
            while ($row = $stmt->fetchObject()) {
                $usuarioModel = new UsuarioModel;
                $usuarioModel->setId($row->ID);
                $usuarioModel->setNome($row->NOME);
                $usuarioModel->setUsername($row->USERNAME);
                $usuarioModel->setSenha($row->SENHA);
                $results[] = $usuarioModel;
            }
        }
        return $results;
    }

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM usuario
            WHERE id = :id
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $usuarioModel = new UsuarioModel;
            $usuarioModel->setId($row->ID);
            $usuarioModel->setNome($row->NOME);
            $usuarioModel->setUsername($row->USERNAME);
            $usuarioModel->setSenha($row->SENHA);
        }
        return $usuarioModel;
    }

		public function getByUsername($username) {
			$stmt = $this->conn->prepare("
				SELECT *
					FROM usuario
					WHERE username = :username
			");
			$stmt->bindValue(':username', $username);
			$stmt->execute();
			if ($stmt) {
					$row = $stmt->fetchObject();
					$usuarioModel = new UsuarioModel;
					$usuarioModel->setId($row->ID);
					$usuarioModel->setNome($row->NOME);
					$usuarioModel->setUsername($row->USERNAME);
					$usuarioModel->setSenha($row->SENHA);
			}
			return $usuarioModel;
		}

    public function insert(UsuarioModel $usuarioModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO usuario (nome, username, senha)
                VALUES (:nome, :username, :senha)
            ");
            $stmt->bindValue(':nome', $usuarioModel->getNome());
            $stmt->bindValue(':username', $usuarioModel->getUsername());
            $stmt->bindValue(':senha', $usuarioModel->getSenha());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(UsuarioModel $usuarioModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE usuario
                SET nome = :nome,
                    username = :username,
                    senha = :senha
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $usuarioModel->getId());
            $stmt->bindValue(':nome', $usuarioModel->getNome());
            $stmt->bindValue(':username', $usuarioModel->getUsername());
            $stmt->bindValue(':senha', $usuarioModel->getSenha());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM usuario
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }
        return true;
    }

}
