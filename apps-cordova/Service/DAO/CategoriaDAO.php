<?php
require_once "DAO.php";

class CategoriaDAO extends DAO {

    public function getAll() {
        $results = array();
        $stmt = $this->conn->query("
          SELECT *
            FROM categoria
        ");
        if ($stmt) {
            while ($row = $stmt->fetchObject()) {
                $categoriaModel = new CategoriaModel;
                $categoriaModel->setId($row->ID);
                $categoriaModel->setTitulo($row->TITULO);
                $categoriaModel->setDescricao($row->DESCRICAO);
                $categoriaModel->setCor($row->COR);
                $results[] = $categoriaModel;
            }
        }
        return $results;
    }

    public function getById($id) {
        $stmt = $this->conn->prepare("
          SELECT *
            FROM categoria
            WHERE id = :id
        ");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        if ($stmt) {
            $row = $stmt->fetchObject();
            $categoriaModel = new CategoriaModel;
            $categoriaModel->setId($row->ID);
            $categoriaModel->setTitulo($row->TITULO);
            $categoriaModel->setDescricao($row->DESCRICAO);
            $categoriaModel->setCor($row->COR);
        }
        return $categoriaModel;
    }

    public function insert(CategoriaModel $categoriaModel) {
        try {
            $stmt = $this->conn->prepare("
              INSERT INTO categoria (titulo, descricao, cor)
                VALUES (:titulo, :descricao, :cor)
            ");
            $stmt->bindValue(':titulo', $categoriaModel->getTitulo());
            $stmt->bindValue(':descricao', $categoriaModel->getDescricao());
            $stmt->bindValue(':cor', $categoriaModel->getCor());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function update(CategoriaModel $categoriaModel) {
        try {
            $stmt = $this->conn->prepare("
              UPDATE categoria
                SET titulo = :titulo,
                    descricao = :descricao,
                    cor = :cor
                WHERE id = :id
            ");
            $stmt->bindValue(':id', $categoriaModel->getId());
            $stmt->bindValue(':titulo', $categoriaModel->getTitulo());
            $stmt->bindValue(':descricao', $categoriaModel->getDescricao());
            $stmt->bindValue(':cor', $categoriaModel->getCor());
            $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function delete($id) {
        $this->conn->beginTransaction();
        try {
            $stmt = $this->conn->prepare('
                DELETE FROM categoria
                WHERE id = :id
            ');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            $this->conn->commit();
        } catch (Exception $e) {
            $this->conn->rollback();
        }
        return true;
    }

}
