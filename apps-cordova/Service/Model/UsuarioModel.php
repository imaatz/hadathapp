<?php
require_once "Model.php";

class UsuarioModel extends Model {

    private $id;
		private $nome;
    private $username;
		private $senha;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

		public function getNome() {
				return $this->nome;
		}
		public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getUsername() {
        return $this->username;
    }
	  public function setUsername($username) {
        $this->username = $username;
    }

		public function getSenha() {
				return $this->senha;
		}
		public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function valida() {
        $error = false;

        return $error;
    }

}
