<?php
require_once "Model.php";

class ConfirmacaoPresencaModel extends Model {

		private $id;
    private $idUsuario;
    private $idEvento;
    private $presenca;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }
    public function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }

    public function getIdEvento() {
        return $this->idEvento;
    }
    public function setIdEvento($idEvento) {
        $this->idEvento = $idEvento;
    }

    public function getPresenca() {
        return $this->presenca;
    }
    public function setPresenca($presenca) {
        $this->presenca = $presenca;
    }

    public function valida() {
        $error = false;

        return $error;
    }

}
