<?php
require_once "Model.php";

class EventoModel extends Model {

    private $id;
		private $titulo;
    private $descricao;
		private $datahora;
		private $idCategoria;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
		public function getTitulo() {
				return $this->titulo;
		}
		public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }
    public function getDescricao() {
        return $this->descricao;
    }
	  public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }
		public function getDatahora() {
				return $this->datahora;
		}
		public function setDatahora($datahora) {
        $this->datahora = $datahora;
    }
		public function getIdCategoria() {
				return $this->idCategoria;
		}
		public function setIdCategoria($idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    public function valida() {
        $error = false;
        return $error;
    }
}
