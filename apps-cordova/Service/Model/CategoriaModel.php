<?php
require_once "Model.php";

class CategoriaModel extends Model {

    private $id;
		private $titulo;
    private $descricao;
		private $cor;

    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }

		public function getTitulo() {
				return $this->titulo;
		}
		public function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    public function getDescricao() {
        return $this->descricao;
    }
	  public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

		public function getCor() {
				return $this->cor;
		}
		public function setCor($cor) {
        $this->cor = $cor;
    }

    public function valida() {
        $error = true;

        return $error;
    }

}
