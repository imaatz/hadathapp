<?php
class Model {
    public function dateConverter($datahora) {
        $data = substr($datahora, 0, 10);
        $hora = substr($datahora, 11, 5);
        $data_formatada = implode("/", array_reverse(explode("-", $data)));
        
        return $data_formatada . ' ' . $hora;
    }

    public function dateCompare($data){
    	date_default_timezone_set('America/Sao_Paulo');
		$data_atual = date('Y-m-d H:i');
    	if(strtotime($data) > strtotime($data_atual)){
    		return true;
    	}
    	return false;
    }
}
?>