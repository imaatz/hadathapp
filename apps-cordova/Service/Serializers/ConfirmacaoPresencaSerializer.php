<?php
class ConfirmacaoPresencaSerializer {

	public function serializeList($confirmacoes) {
		$confirmacoes_array = array();
		foreach ($confirmacoes as $confirmacao) {
			$confirmacoes_array[] = $this->serialize($confirmacao);
		}
		return $confirmacoes_array;
	}

	public function serialize($confirmacaoPresencaModel) {
		return array(
			"id" => $confirmacaoPresencaModel->getId(),
			"idUsuario" => $confirmacaoPresencaModel->getIdUsuario(),
			"idEvento" => $confirmacaoPresencaModel->getIdEvento(),
			"presenca" => $confirmacaoPresencaModel->getPresenca()
		);
	}

}
