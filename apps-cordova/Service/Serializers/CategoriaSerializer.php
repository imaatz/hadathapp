<?php
class CategoriaSerializer {

	public function serializeList($categorias) {
		$categorias_array = array();
		foreach ($categorias as $categoria) {
			$categorias_array[] = $this->serialize($categoria);
		}
		return $categorias_array;
	}

	public function serialize($categoriaModel) {
		return array(
			"id" => $categoriaModel->getId(),
			"titulo" => $categoriaModel->getTitulo(),
			"descricao" => $categoriaModel->getDescricao(),
			"cor" => $categoriaModel->getCor()
		);
	}

}
