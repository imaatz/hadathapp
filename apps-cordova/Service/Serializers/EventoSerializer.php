<?php
class EventoSerializer {

	public function serializeList($eventos) {
		$eventos_array = array();
		foreach ($eventos as $evento) {
			$eventos_array[] = $this->serialize($evento);
		}
		return $eventos_array;
	}

	public function serialize($eventoModel) {
		return array(
			"id" => $eventoModel->getId(),
			"titulo" => $eventoModel->getTitulo(),
			"descricao" => $eventoModel->getDescricao(),
			"datahora" => $eventoModel->getDatahora(),
			"idCategoria" => $eventoModel->getIdCategoria()
		);
	}

}
