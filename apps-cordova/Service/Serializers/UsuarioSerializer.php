<?php
class UsuarioSerializer {

	public function serializeList($usuarios) {
		$usuarios_array = array();
		foreach ($usuarios as $usuario) {
			$usuarios_array[] = $this->serialize($usuario);
		}
		return $usuarios_array;
	}

	public function serialize($usuarioModel) {
		return array(
			"id" => $usuarioModel->getId(),
			"nome" => $usuarioModel->getNome(),
			"username" => $usuarioModel->getUsername(),
			"senha" => $usuarioModel->getSenha()
		);
	}

}
